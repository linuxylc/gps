    #include <stdio.h>  
    #include <termios.h>  
    #include <strings.h>  
    #include <string.h>  
    #include <stdlib.h>  
    #include <fcntl.h>  
    #include <unistd.h>  


    #define MAX_PACKET_SIZE 1024  





    static void setTermios(struct termios * pNewtio, int uBaudRate)  
    {  
        bzero(pNewtio, sizeof(struct termios)); /* clear struct for new port settings */  
        //8N1  
        pNewtio->c_cflag = uBaudRate | CS8 | CREAD | CLOCAL;  
        pNewtio->c_iflag = IGNPAR;  
        pNewtio->c_oflag = 0;  
        pNewtio->c_lflag = 0; //non ICANON  
    }  
      



    void um220_uart_init(int ttyFd,struct termios *oldtio,struct termios *newtio)  
    {  
      
      
        tcgetattr(ttyFd, oldtio); /* save current serial port settings */  
        setTermios(newtio, B9600);  
        tcflush(ttyFd, TCIFLUSH);  
        tcsetattr(ttyFd, TCSANOW, newtio);  
    }  
      
    void parseData(char *buf)  
    {  
        int nQ, nN, nB, nC;  
        char cX, cY, cM1, cM2;  
        float fTime, fX, fY, fP, fH, fB, fD; // 
      
        if (buf == NULL)  
        {  
            printf("error: Can't get buf!\n");  
            return;  
        }  
        sscanf(buf,"$GNGGA,%f,%f,%c,%f,%c,%d,%02d,%f,%f,%c,%f,%c,%f,%04d%02x",&fTime,&fX,&cX,&fY,&cY,&nQ,&nN,&fP,&fH,&cM1,&fB,&cM2, &fD, &nB, &nC);  
      
        printf("x: %c %f, y: %c %f, h %f, satellite: %d\n",cX, fX, cY, fY, fH, nN);  


    /*cX：N or S;fX：纬度；cY：E or W；fY：经度；fH：height；nN：卫星个数*/  
    }  
      
    int main(int argc, char *argv[])  
    {  
        int nb,command;  
        int um220_fd = -1;  
        char newbuf[MAX_PACKET_SIZE];  
        char msg[20],*ret=NULL;  
        struct termios oldtio, newtio;  
      
    {/* usage */
        if (argc != 2) {
            printf("%s nod  eg:gpstest /dev/ttyUSB0\n", argv[0]);
            exit(-1);
        }
    }//


        /*Open Um220 Module*/  
        if ((um220_fd = open(argv[1], O_RDWR)) < 0) {  
            printf("error: Can't open serial port %s!\n", argv[1]);  
                    return -1;  
            }  
      
        /*Init Uart for Um220*/  
        um220_uart_init(um220_fd,&oldtio,&newtio);  
          
        /*Set Um220 options*/  
        printf("Please select modules of um220\n");  
        printf("1.BD module\n");  
        printf("2.GPS module\n");  
        printf("3.BD+GPS module\n");  
        if(scanf("%d",&command) != 1)  
        {  
            printf("error:input is wrong!\n");  
        }  
        switch(command)  
        {  
            case 1:  
                memset(msg, 0, sizeof(msg));  
                strcpy(msg,"$cfgsys,h01");  
                if(write(um220_fd,msg,sizeof(msg)) < 0)  
                printf("Failed to set BD modules!\n");  
                break;  
            case 2:  
                memset(msg, 0, sizeof(msg));  
                strcpy(msg,"$cfgsys,h10");  
                if(write(um220_fd,msg,sizeof(msg)) < 0)  
                printf("Failed to set GPS modules!\n");  
                break;  
            case 3:  
                memset(msg, 0, sizeof(msg));  
                strcpy(msg,"$cfgsys,h11");  
                if(write(um220_fd,msg,sizeof(msg)) < 0)  
                printf("Failed to set BD+GPS modules!\n");  
                break;  
            default:  
                printf("Can't identify command,set BD+GPS modules!\n");  
                memset(msg, 0, sizeof(msg));  
                strcpy(msg,"$cfgsys,h11");  
                if(write(um220_fd,msg,sizeof(msg)) < 0)  
                printf("Failed to set BD+GPS modules!\n");  
        }  
      
      
        for(;;)  
        {  
        /*Read Data from Um220*/  
        memset(newbuf, 0, 1024);  
        nb = read(um220_fd, newbuf, MAX_PACKET_SIZE);  
            if (nb == -1)  
            {  
                    perror("read uart error");  
            return -1;  
            }  
        if ((ret=strstr(newbuf, "$GNGGA")) != NULL)  
            {  
        /*Analysis Data*/  
            parseData(ret);  
            }  
        sleep(1);  
        }  
        /*Recover Settings Of Serial Port*/  
        tcsetattr(um220_fd,TCSANOW,&oldtio);  
        /*Close Um220_fd*/  
        close(um220_fd);  
	return 0;
	}
